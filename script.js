const example = document.getElementById('example');
const togglePrice = document.getElementById('togglePrice');

example.addEventListener('change', (event) => {
    togglePrice.classList.toggle('priceToggle');
});